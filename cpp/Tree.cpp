#include "Tree.h"

double Tree::likelihood(vector<double>& K, vector<double>& beta, 
                        vector<double>& mu, vector<double>& psi, 
                        double rho, int est_norm, int vflag, 
                        int rescale, int surv, int model) 
{
  double fx(-INFINITY);

  switch (model) {
    case 0:
      break;
    case 2:
      fx = expoTreeSIR(K.size(),K.data(),beta.data(),mu.data(),psi.data(),
                       rho,times,ttypes,extant+nroot,est_norm,
                       vflag,rescale,nroot);
      break;
    case 1:
    default:
      fx = expoTreeEval(K,beta,mu,psi,rho,times,ttypes,extant,est_norm,vflag,rescale);
      break;
  }

  if (surv) {
    fx -= survival(K,beta,mu,psi,rho,est_norm,vflag,rescale,nroot,model);
  }

  return fx;
}

// ==========================================================================

double Tree::survival(vector<double>& K, vector<double>& beta, 
    vector<double>& mu, vector<double>& psi, double rho,
    int est_norm, int vflag, int rescale, int nroot, int model_type)
{
  double fx(-INFINITY);

  vector<int> survTypes(ttypes.size(),99);
  for (size_t i(0); i < ttypes.size(); ++i) {
    switch (ttypes[i]) {
      case 3:
        survTypes[i] = 3;
        break;
      default:
        break;
    }
  }

  switch (model_type) {
    case 0:
      break;
    case 2:
      fx = expoTreeSIRSurvival(K.size(),K.data(),beta.data(),mu.data(),
                      psi.data(),rho,times,ttypes,extant+nroot,est_norm,
                      vflag,rescale,nroot);
      break;

    case 1:
    default:
      fx = expoTreeSurvival(K,beta,mu,psi,rho,times,survTypes,
                            extant,est_norm,vflag,rescale);
  }
  return fx;
}

// ==========================================================================

void Tree::addRateShift(double t) {
  // find position
  size_t pos(0);
  do {
    if (times[pos] > t) break;
    ++pos;
  } while (pos < times.size());
  vector<double>::iterator p1(times.begin());
  vector<int>::iterator p2(ttypes.begin());
  p1 += pos;
  p2 += pos;
  times.insert(p1,t);
  ttypes.insert(p2,20);
}


