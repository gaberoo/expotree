/*
 * Copyright (c) 2012-2014, Gabriel Leventhal, ETH Zurich
 * All rights reserved.
 *
 * Original MATLAB implementation:
 * Copyright (c) 2010, Nick Higham and Awad Al-Mohy
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer
 *     in the documentation and/or other materials provided with the
 *     distribution.
 *   * Neither the name of the ETH Zurich nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "expmv.h"

inline void exchange(double** x, double** y) {
  double* tmp = *x;
  *x = *y;
  *y = tmp;
}

inline void exchange_cpy(int n, double* x, double* y) {
  double* tmp = (double*) malloc(n*sizeof(double));
  memcpy(tmp,y,n*sizeof(double));
  memcpy(y,x,n*sizeof(double));
  memcpy(x,tmp,n*sizeof(double));
}

void expmv(double t, int n, matMat fmv, normFunc nf, traceFunc tf,
    double* b, int ncol, int m_max, int p_max, double* tm, int recalcm,
    char prec, double shift, char bal, int full_term, int prnt, int* info,
    int est_norm, int wrklen, double* wrk, int iwrklen, int* iwrk, void* pars)
{
  int mv = 0;           /* number of matrix-vector products computed */
  int mvd = 0;
  int unA;

  int i;
  int j;
  int k;

  double tt;
  double tol;       /* tolerance */
  int tcol;         /* size of Taylor approximation */

  int wlen;
  int nalen;

  int   nncol = n*ncol;
  int    ione = 1;
  double done = 1.0;

  /* check supplied memory */
  wlen = 2*p_max + 2*nncol + m_max*(p_max-1);
  nalen = 3*n + (4*n+1)*ncol;
  if (wrklen < wlen+nalen || iwrklen < 2*n + 4) {
#ifdef DEBUG
    if (prnt) {
      Rprintf("Not enough workspace supplied!\n");
      Rprintf("  Required 'double': %d\n",wlen+nalen);
      Rprintf("  Supplied         : %d\n",wrklen);
      Rprintf("  Required 'int'   : %d\n",2*n+4);
      Rprintf("  Supplied         : %d\n",iwrklen);
    }
#endif
    *info = -2;
    return;
  }

  double* talpha = wrk;                  /* length = p_max */
  double* teta   = talpha + p_max;       /* length = p_max */
  double* C      = teta + p_max;         /* length = m_max*(p_max-1) */
  double* b1     = C + m_max*(p_max-1);  /* length = n*ncol */
  double* b2     = b1 + nncol;           /* length = n*ncol */
  double* nawrk  = b2 + nncol;           /* length = nalen = (4*n+1)*ncol + 3*n > n*ncol */

  /* assign tolerance */
  switch (prec) {
    case 's': tol = pow(2.,-24.); break;
    case 'h': tol = pow(2.,-10.); break;
    case 'd': 
    default: tol = pow(2.,-53.); break;
  }

  // get required Taylor truncation (if not set)
  if (recalcm) {
#ifdef DEBUG
    if (prnt > 2) Rprintf("Calculating required Taylor truncation...");
#endif
    tt = 1.;
    select_taylor_degree(n,fmv,t,b,ncol,nf,m_max,p_max,
                         prec,talpha,teta,tm,&mvd,&unA,shift,bal,est_norm,
                         wrklen-wlen,nawrk,iwrklen,iwrk,pars);
#ifdef DEBUG
    if (prnt > 4) {
      for (i = 0; i < m_max; ++i) {
        for (j = 0; j < p_max-1; ++j) {
          Rprintf("%16.8e ",tm[j*m_max+i]);
        }
        Rprintf("\n");
      }
    }
#endif
    mv = mvd;
#ifdef DEBUG
    if (prnt > 2) Rprintf("done.\n");
#endif
  } else {
    tt = t; 
    mv = 0; 
    mvd = 0;
  }

  double s = 1.0;      /* cost per column */
  double cost = 0.0;
  double ccost = 0.0;

  if (t == 0.0) { 
    tcol = 0; 
  } else {
    k = 0;

    for (i = 0; i < m_max; ++i) {
      for (j = 0; j < p_max-1; ++j) {
        C[k] = ceil(fabs(tt)*tm[j*(m_max)+i])*(i+1.);
        if (C[k] == 0.0) C[k] = 1./0.;
        ++k;
      }
    }

    cost = R_PosInf;
    tcol = 0;
    for (i = 0; i < m_max; ++i) {
      ccost = R_PosInf;
      for (j = 0; j < p_max-1; ++j) {
        if (C[i*(p_max-1)+j] < ccost) {
          ccost = C[i*(p_max-1)+j];
        }
      }
      if (ccost < cost) {
        cost = ccost;
        tcol = i+1;
      }
    }

    s = (cost/tcol > 1) ? cost/tcol : 1.0;
  }

  if (tcol == 0) {
    if (prnt) {
      Rprintf("Cannot calculate matrix exponential (under-/overflow?).\n");
      Rprintf("Returned results may be gibberish.\n");
    }
    *info = -1;
    return;
  }

  double eta = 1.0;
  if (shift != 0.0) eta = exp(t*shift/s);

  memcpy(b1,b,nncol*sizeof(double));

#ifdef DEBUG
  if (prnt > 2) Rprintf("m = %2d, s = %g, ||b|| = %g\n", tcol, s, inf_norm(n,ncol,b));
#endif

  double c1 = 0.0;
  double c2 = 0.0;
  double bnorm = 0.0;

  int ss;
  for (ss = 1; ss <= s; ++ss) {
    /* get norm of input vector/matrix */
    c1 = inf_norm(n,ncol,b1);

#ifdef DEBUG
    if (prnt > 3) Rprintf("s = %d, ",ss);
    if (prnt > 4) Rprintf("\n");
#endif

    for (k = 1; k <= tcol; ++k) {
      /* apply matrix to vector, but scale by t/(s*k) */
      fmv('n',n,ncol,t/(s*k),b1,b2,pars);
      ++mv;  /* increment matrix-vector product counter */

      /* norm of output vector */
      c2 = inf_norm(n,ncol,b2);

#ifdef DEBUG
      if (prnt > 4) Rprintf("k=%3d: |b| = %9.2e, c1 = %9.2e, c2 = %9.2e.",k,bnorm,c1,c2);
#endif

      /* add b2 onto b */
      daxpy_(&nncol,&done,b2,&ione,b,&ione);

      /* check whether convergence has been reached before full truncation */
      if (! full_term) {
        /* get new infinite norm of the new b */
        bnorm = inf_norm(n,ncol,b);
        
#ifdef DEBUG
        if (prnt > 4) Rprintf(" |b'| = %9.2e, (c1+c2)/|b| = %9.2e.\n",bnorm,(c1+c2)/bnorm);
#endif

        if (c1+c2 <= tol*bnorm) {
#ifdef DEBUG
          if (prnt == 4) {
            Rprintf("k=%3d: %9.2e %9.2e %9.2e",k,bnorm,c1,c2);
            Rprintf(" %9.2e, ",(c1+c2)/bnorm);
          }
          if (prnt > 3) Rprintf("m_actual = %2d\n", k);
#endif
          break;
        }

        c1 = c2;
      }

      /* exchange b1 and b2 */
      exchange(&b1,&b2);
    }

    for (i = 0; i < nncol; ++i) b[i] *= eta;
    memcpy(b1,b,nncol*sizeof(double));
  }

  iwrk[0] = tcol;
  iwrk[1] = k;
  iwrk[2] = mv;

  *info = 1;
}

/****************************************************************************/
 
double find_absmax_array(int i0, int len, const double* x) {
  if (len > 1) {
    int i3 = i0+len/2;
    return fmax(find_absmax_array(i0,i3,x),find_absmax_array(i3+1,i0+len,x));
  } else if (len == 1) {
    return fmax(fabs(x[i0]),fabs(x[i0+len]));
  } else {
    return fabs(x[i0]);
  }
}

/****************************************************************************/

double inf_norm(int n1, int n2, double* A) {
  double c = 0.0;
#if defined(UNROLL)
  /* use unrolled loops */
  int i;
  double rowsum[4];
  int m = n1 % 4;
  if (m != 0) {
    for (i = 0; i < m; ++i) {
      rowsum[i] = fabs(A[i]);
      if (rowsum[i] > c) c = rowsum[i];
    }
  }
  if (n1 < 4) return c;
  int j1, j2;
  for (i = m; i < n1; i += 4) {
    rowsum[0] = fabs(A[i]);
    rowsum[1] = fabs(A[i+1]);
    rowsum[2] = fabs(A[i+2]);
    rowsum[3] = fabs(A[i+3]);
    j1 = (rowsum[0] > rowsum[1]) ? 0 : 1;
    j2 = (rowsum[2] > rowsum[3]) ? 2 : 3;
    if (rowsum[j2] > rowsum[j1]) j1 = j2;
    if (rowsum[j1] > c) c = rowsum[j1];
  }
#elif defined(IDAMAX)
  int one = 1;
  int row = idamax_(&n1,A,&one);
  c = A[row];
#elif defined(DLANGE)
  char infNormChar = 'i';
  c = dlange_(&infNormChar,&n,&ncol,b1,&n,nawrk);
#else
  double rowsum;
  int i;
  for (i = 0; i < n1; ++i) {
    // rowsum = 0.0;
    // int j;
    // for (j = 0; j < n2; ++j) rowsum += fabs(A[j*n1+i]);
    rowsum = fabs(A[i]);
    if (rowsum > c) c = rowsum;
  }
#endif
  return c;
}


