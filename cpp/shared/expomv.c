/*
 * Copyright (c) 2012-2014, Gabriel Leventhal, ETH Zurich
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer
 *     in the documentation and/or other materials provided with the
 *     distribution.
 *   * Neither the name of the ETH Zurich nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "expomv.h"

/************************************************************/ 

void rExpoTree(double* RN, int* Rki, double* Rbeta, double* Rmu,
    double* Rpsi, int* Rn, int* parVecLen, double* times, 
    int* ttypes, double* p, double* t0, int* info, 
    int* estimateNorm, int* Rvflag, int* Rrescale) 
{
  /* allocate parameter structure and initialize */
  expo_type* expo = expo_type_alloc();

  expo->N       = (int) ceil(RN[0]); /* total population size */
  expo->K       = RN[0];             /* carrying capacity */
  expo->ki      = *Rki;              /* current number of lineages */
  expo->beta    = Rbeta[0];          /* branching rate */
  expo->mu      = Rmu[0];            /* extinction rate */
  expo->psi     = Rpsi[0];           /* sampling rate */
  expo->rescale = *Rrescale;         /* rescale probability vector */
  expo->vflag   = *Rvflag;           /* verbosity level */
  expo->cutoff  = 0.0;               /* precision for zero */

  expo->parVecLen = *parVecLen;      /* number of parameter sets */
  expo->curPar  = 0;                 /* current parameter set */
  expo->NVec    = RN;                /* N parameters */
  expo->betaVec = Rbeta;             /* beta parameters */
  expo->muVec   = Rmu;               /* mu parameters */
  expo->psiVec  = Rpsi;              /* psi parameters */

  expo->offset = 0;                  /* don't calculate for zero entries */
  expo->est_norm = *estimateNorm;    /* force estimation of matrix norm */

  int n = *Rn;                       /* number of events in the tree */

  /* get maximum carrying capacity */
  expo->N_max = max_pop_size(expo);
  expo->dim = expo->N_max+1;

  /* allocate workspace for matrix */
  expo->mat = (double*) malloc(3*expo->dim*sizeof(double));

  /* choose lambda function */
  switch (expo->SImodel) {
    case 0:
      expo->lambda = &lambdaInf;
      break;
    default:
      expo->lambda = &lambdaSI;
      break;
  }

  /* allocate workspace for lambda */
  expo->lambdaVec = (double*) malloc(expo->dim*sizeof(double));
  expo->init_all(expo);

  int ncol = 1;                  /* EXPMV parameters */

  /* calculate required memory and allocate */
  int memlen = (2*expo->dim+(expo->p_max-1)*expo->m_max);
  int wlen   = 2*expo->p_max + (6*ncol+3)*expo->dim + ncol + expo->m_max*(expo->p_max-1);
  int iwlen  = 2*expo->dim + 4;

  double* wrk = (double*) malloc((memlen+wlen)*sizeof(double));
  int* iwrk = (int*) malloc(iwlen*sizeof(int));

  /* set present time */
  wrk[0] = *t0;

  /* allocate workspace for history */
  // expo->p_save = (double*) malloc(n*expo->dim*sizeof(double));

  /* call algorithm */
  expoTree(n,times,ttypes,p,memlen+wlen,wrk,iwlen,iwrk,expo);
  *info = iwrk[0];

  /* clean up */
  // free(expo->p_save);

  free(expo->mat);
  expo->mat = NULL;

  free(expo->lambdaVec);
  expo->lambdaVec = NULL;

  free(iwrk);
  free(wrk);

  expo_type_free(expo);
}

/************************************************************/ 

void expoTree(int n, double* times, int* ttypes, 
    double* p, int wrklen, double* wrk, int iwrklen, int* iwrk,
    expo_type* expo) 
{
  /***************** SI PARAMETERS *****************/

  double t = 0.0;           /* total time */
  double dt = 0.0;          /* time step */

  double nrm = 0.0;         /* vector norm */
  double scale = 0.0;       /* log scaling factor */
  
  int info = 0;             /* info variable */
  int one = 1;              /* integer 1 (FORTRAN corpse) */

  int ncol = 1;             /* EXPMV parameters */

  /********************* EXPMV *********************/

  int N = expo->dim;        /* maximum dimension */

  /* split up calculation when precision is bad */
  int    wrk_steps     = 1;
  double wrk_dt        = 0.0;
  int    wrk_info      = 0;
  double wrk_scale     = 0.0;
  int    max_wrk_steps = 1000;

  /* calculate required memory and check */
  int memlen = (2*N+(expo->p_max-1)*expo->m_max);
  int wlen   = 2*expo->p_max + (6*ncol+3)*N + ncol + expo->m_max*(expo->p_max-1);
  int iwlen  = 2*N + 4;
  if (wrklen < memlen+wlen || iwrklen < iwlen) {
    Rprintf("Not enough memory supplied!");
    return;
  }

  /* set pointers for easier access to variables */
  double* p0 = wrk;
  double* pT = wrk + N;
  double* tm = wrk + 2*N;
  double* expowrk = wrk + memlen;

  /* verbose output */
  if (expo->vflag > 0) {
    Rprintf("Running expoTree with parameters:\n");
    Rprintf(" n    = %d\n",n);
    Rprintf(" |v|  = %f\n",dnrm2_(&N,p,&one));
    Rprintf(" ki   = %d\n",expo->ki);
    Rprintf(" Nmax = %d\n",N);
    int i;
    Rprintf("   K    =");
    for (i = 0; i < expo->parVecLen; ++i) Rprintf(" %8.2f",expo->NVec[i]);
    Rprintf("\n");
    if (! expo->user_funcs) {
      Rprintf("   beta =");
      for (i = 0; i < expo->parVecLen; ++i) Rprintf(" %8.2f",expo->betaVec[i]);
      Rprintf("\n   mu   =");
      for (i = 0; i < expo->parVecLen; ++i) Rprintf(" %8.2f",expo->muVec[i]);
      Rprintf("\n   psi  =");
      for (i = 0; i < expo->parVecLen; ++i) Rprintf(" %8.2f",expo->psiVec[i]);
      Rprintf("\n");
    } else {
      Rprintf("Running with user-supplied functions.\n");
    }
  }

  /******** INTEGRATE DIFFERENTIAL EQUATION ********/

  /* initialize variables */
  t = wrk[0];
  scale = 0.0;
  nrm = 1.0;

  /* copy probability vector and calculate norm */
  memcpy(p0,p,N*sizeof(double));
  nrm = dnrm2_(&N,p0,&one);

  /* rescale probability vector if requested */
  if (expo->rescale && nrm != 1.0) {
    scale += log(nrm);
    nrm = 1./nrm;
    dscal_(&N,&nrm,p0,&one);
    nrm = dnrm2_(&N,p0,&one);
  }

  /* loop over tree events */
  int i, j;

  /* set unused elements to zero */
  // for (j = Ncur; j < N; ++j) p0[j] = 0.0;

  for (i = 0; i < n; ++i) {
    /* get time interval between events */
    dt = times[i] - t;

    if (dt < 0.0) {
      Rprintf("Negative dt (%f) at time step %d! Aborting.\n",dt,i);
      Rprintf("Numer of time points: n = %d\n",n);
      Rprintf("t(0)   = %8.4e\n",times[0]);
      Rprintf("t(i)   = %8.4e\n",t);
      Rprintf("t(i+1) = %8.4e\n",times[i]);
      Rprintf("dt     = %8.4e\n",dt);
      reset_p(p,expo);
      iwrk[0] = -1;
      return;
    }

    /* don't do anything for dt = 0 */
    if (dt > 0.0) {
      wrk_steps = 1;
      wrk_info = 0;

      /* TODO: is it really necessary to init_all ? Or only the shift and the
       * matrix ? */
      expo->init_all(expo);

      /* save a backup of the vector */
      memcpy(pT,p0,N*sizeof(double));

      while (wrk_info == 0) {
        if (wrk_steps > max_wrk_steps) {
          Rprintf("Maximum time step intervals reached.\n");
#ifdef DEBUG
          if (! expo->user_funcs) {
            for (j = 0; j < N; ++j) p[j] = R_NegInf;
            for (j = 0; j < expo->parVecLen; ++j) Rprintf(" -N %g",expo->NVec[j]);
            for (j = 0; j < expo->parVecLen; ++j) Rprintf(" -b %g",expo->betaVec[j]);
            for (j = 0; j < expo->parVecLen; ++j) Rprintf(" -u %g",expo->muVec[j]);
            for (j = 0; j < expo->parVecLen; ++j) Rprintf(" -s %g",expo->psiVec[j]);
            for (j = 0; j < n; ++j) if (ttypes[j] == 20) Rprintf(" -S %g",times[j]);
            Rprintf("\n");
          }
#endif
          reset_p(p,expo);
          iwrk[0] = -2;
          return;
        }

        wrk_dt = (wrk_steps > 1) ? dt/wrk_steps : dt;
        wrk_scale = 0.0;

        int k;
        for (k = 0; k < wrk_steps; ++k) {
          info = 0;

          expmv(wrk_dt,N,expo->matvec,expo->norm,expo->trace,p0+expo->offset,
                1,expo->m_max,expo->p_max,tm,1,'d',expo->shift,0,0,expo->vflag,&info,
                expo->est_norm,wrklen-memlen,expowrk,iwrklen,iwrk,expo);

          // printf("number of matrix-vector products = %d\n",iwrk[2]);

          // Error during calculation. Return INF.
          if (info < 0) {
            // Rprintf("Error in 'expmv': info = %d\n",info);
            reset_p(p,expo);
            iwrk[0] = -3;
            return;
          }

          // check for negative values
          int found_neg = 0;
          for (j = 0; j < N; ++j) {
            if (p0[j] < 0.0 || ! isfinite(p0[j])) {
              // printf(" --- m = %d, p = %g\n",j,p0[j]);
              ++found_neg;
              p0[j] = 0.0;
            }
          }

          if (found_neg && expo->vflag > 1) {
            Rprintf("DEBUG: %d negative probabilities found! info = %d\n",found_neg,info);
            nrm = dnrm2_(&N,p0,&one);
            Rprintf("%6d/%02d %8.4f/%8.4f %5d %10.4e % 11.4e | %2d %2d\n",
                    i,ttypes[i],dt,times[i],expo->ki,nrm,scale+wrk_scale,iwrk[0],iwrk[1]);
            exit(1);
          }

          // calculate norm of vector
          nrm = dnrm2_(&N,p0,&one);

          // validate 2-norm of the vector
          if (nrm < expo->cutoff || isnan(nrm)) {
            if (expo->vflag > 1) Rprintf("Vector norm invalid. Aborting.\n");
            reset_p(p,expo);
            iwrk[0] = -4;
            return;
          }

          if (expo->rescale) {
            wrk_scale += log(nrm);
            nrm = 1./nrm;
            dscal_(&N,&nrm,p0,&one);
          }

          if (expo->vflag > 1) {
            if (wrk_steps == 1) {
              Rprintf("%6d/%02d %8.4f/%8.4f %5d %10.4e % 11.4e | %2d %2d\n",
                  i,ttypes[i],dt,times[i],expo->ki,nrm,scale+wrk_scale,iwrk[0],iwrk[1]);
            } else {
              Rprintf("%8d %8f %5d %8.4e % 8.4e %2d %2d\n",
                  k,wrk_dt,expo->ki,nrm,scale+wrk_scale,iwrk[0],iwrk[1]);
            }
          }

          if (((iwrk[0] > 40 && iwrk[0] < iwrk[1]) && wrk_steps < max_wrk_steps) || found_neg > 0) {
            memcpy(p0,pT,N*sizeof(double));
            ++wrk_steps;
            wrk_info = 0;
            if (expo->vflag > 2) {
              Rprintf("Decreasing time step (%d).\n",wrk_steps);
            }
            break;
          } else {
            wrk_info = 1;
          }
        }
      }

      if (expo->rescale) scale += wrk_scale;
    } else {
      nrm = dnrm2_(&N,p0,&one);
      if (expo->vflag > 1) {
        wrk_scale = 0.0;
        Rprintf("%6d/%02d %8.4f/%8.4f %5d %10g\n",
                i,ttypes[i],times[i],dt,expo->ki,nrm);
      }
    }

    // update initial condition with event information
    if (i < n-1) {
      switch (ttypes[i]) {
        case 1:
          // branching event
          (*expo->ft)(p0,pT,expo);
          --expo->ki;
          break;

        case 0:
          // sampling-removal event
          (*expo->fs)(p0,pT,expo);
          ++expo->ki;
          break;

        case 2:
          // sample extinct lineage
          ++expo->ki;
        case 3:
          // sampling extant lineage
          (*expo->fs2)(p0,pT,expo);
          break;

        case 4:
          // add lineages without altering likelihood
          memcpy(pT,p0,N*sizeof(double));
          pT[expo->ki] = 0.0;
          if (++expo->ki > expo->N) {
            if (expo->vflag > 1) Rprintf("Increasing lineages above current maximum (N = %d). Aborting.\n",expo->N);
            reset_p(p,expo);
            iwrk[0] = -5;
            return;
          }
          break;

        case 10:
          /* shift vector upwards without altering likelihood */
          memcpy(pT+1,p0,(N-1)*sizeof(double));
          pT[0] = 0.0;
          ++expo->ki;
          break;

        case 11:
          /* shift vector downwards without altering likelihood */
          memcpy(pT,p0+1,(N-1)*sizeof(double));
          pT[N-1] = 0.0;
          --expo->ki;
          break;

        /**** RATE SHIFTS ****/
        case 20: /* immediate extinction */
        case 21:
        case 22:
          if (++expo->curPar < expo->parVecLen) {
            if (expo->vflag > 1) {
              Rprintf("Rate shift!\n");
              if (! expo->user_funcs) {
                Rprintf("  OLD: N = %f, beta = %f, mu = %f, psi = %f, ki = %d\n",
                        expo->K,expo->beta,expo->mu,expo->psi,expo->ki);
              }
              Rprintf("    +++ %g %g %g %g\n",p0[expo->ki-2],p0[expo->ki-1],p0[expo->ki],p0[expo->ki+1]);
            }

            if (! expo->user_funcs) {
              expo->K    = expo->NVec[expo->curPar];
              expo->N    = (int) ceil(expo->K);
              expo->beta = expo->betaVec[expo->curPar];
              expo->mu   = expo->muVec[expo->curPar];
              expo->psi  = expo->psiVec[expo->curPar];
            } else {
              expo->lambdaVec += expo->N_max+1;
              expo->muFun += expo->N_max+1;
              expo->psiFun += expo->N_max+1;
            }

            if (expo->vflag > 1) {
              nrm = dnrm2_(&N,p0,&one);
              if (! expo->user_funcs) {
                Rprintf("  NEW: N = %f, beta = %f, mu = %f, psi = %f. |v| = %f.\n",
                        expo->K,expo->beta,expo->mu,expo->psi,nrm);
              }
              Rprintf("    +++ %g %g %g %g\n",p0[expo->ki-2],p0[expo->ki-1],p0[expo->ki],p0[expo->ki+1]);
            }
          } else {
            if (expo->vflag > 1) {
              Rprintf("Not enough parameters supplied to shift rates. Ignoring rate shift!\n");
            }
          }

          memcpy(pT,p0,N*sizeof(double));
          break;

        case 99:
        default:
          memcpy(pT,p0,N*sizeof(double));
          break;
      }
      memcpy(p0,pT,N*sizeof(double));

      if (expo->p_save != NULL) memcpy(expo->p_save+i*N,pT,N*sizeof(double));
    }

    nrm = dnrm2_(&N,p0,&one);

    if (nrm <= 0.0) {
      if (expo->vflag > 2) {
        Rprintf("Vector norm zero. Illegal event.\n");
      }
      reset_p(p,expo);
      iwrk[0] = -6;
      return;
    }

    /* rescale likelihoods for numerical reasons */
    if (expo->rescale) {
      if (nrm > 1e20) {
        Rprintf("Problem with 2-norm of vector in rescaling!\n");
        Rprintf("Most likely ki >= N.\n");
        Rprintf("||pT|| = 0.0\n");
        Rprintf("||p0|| = %14.8e\n",dnrm2_(&N,p0,&one));
        if (expo->vflag > 2) {
          for (j = 0; j < N; ++j) {
            Rprintf("  pT(%3d) = %12.6e\n",i,pT[j]);
          }
        }
        reset_p(p,expo);
        iwrk[0] = -7;
        return;
      }
      scale += log(nrm);
      nrm = 1./nrm;
      dscal_(&N,&nrm,p0,&one);
    }

    t = times[i];
  }

  for (j = 0; j < N; ++j) {
    /* WAS ERROR PREVIOUSLY!! A PROBABILITY DENSITY CAN BE > 1 !!! */
    // p[j] = ((p0[j] < 1.0) ? log(p0[j]) : 0.0) + scale;
    p[j] = log(p0[j]) + scale;
  }

  if (expo->vflag > 1) {
    Rprintf("p0(%d) = %g, scale = %g\n",1,p0[1],scale);
    Rprintf(" p(%d) = %g\n",1,p[1]);
  }
  
  iwrk[0] = 1;
}

/************************************************************/ 

void reset_p(double* p, const expo_type* expo) {
  int N = expo->N_max+1;
  int j;
  for (j = 0; j < N; ++j) p[j] = R_NegInf;
}

/************************************************************/ 

void printSaved(int n, const expo_type* expo) {
  size_t i, j;
  size_t N = expo->N_max+1;
  for (i = 0; i < n; ++i) {
    for (j = 0; j < N; ++j) {
      printf("%12g ",expo->p_save[i*N+j]);
    }
    printf("\n");
  }
}

