#include "shared/sir.h"

int main(int argc, char** argv) {
  expo_type* pars = (expo_type*) malloc(sizeof(expo_type));
  init_expo(pars);

  pars->N_max = (argc > 1) ? atoi(argv[1]) : 10;

  int I, R;
  for (I = 0; I <= pars->N_max; ++I) {
    for (R = 0; R <= pars->N_max-I; ++R) {
      printf("(%2d,%2d) ",I,R);
    }
    printf("\n");
  }

  for (I = 0; I <= pars->N_max; ++I) {
    for (R = 0; R <= pars->N_max-I; ++R) {
      printf("%4d    ",sir_index(I,R,pars));
    }
    printf("\n");
  }

  printf("Max index = %d\n",sir_index_N(0,pars->N_max,pars->N_max));

  return 0;
}

